<?php
require_once 'Config/db.php';

$members = [
    [
        'fullName' => 'Beate Ulrich',
        'phone' => 1668112,
        'email' => 'ghswinger7@yopmail.com',
        'role' => 'student',
        'averageMark' => 5.1,
        'subject' => 'marketing',
        'workingDay' => '19-21'
    ],[
        'fullName' => 'Olivia C. Hope',
        'phone' => 5994298,
        'email' => 'bcolivia2@yopmail.com',
        'role' => 'teacher',
        'averageMark' => 0 ,
        'subject' => 'marketing',
        'workingDay' => '19-21'
    ],[
        'fullName' => 'Anthony I. Swarts',
        'phone' => 9446184,
        'email' => 'jqkerry-anne16@yopmail.com',
        'role' => 'admin',
        'averageMark' => 0 ,
        'subject' => '',
        'workingDay' => '08-17'
    ],[
        'fullName' => 'Toney E. Copp',
        'phone' => 9007945,
        'email' => 'decopp4@yopmail.com',
        'role' => 'student',
        'averageMark' => 9.3,
        'subject' => 'php',
        'workingDay' => '19-21'
    ],[
        'fullName' => 'Max X. Charon',
        'phone' => 994501,
        'email' => 'ixcharon23@yopmail.com',
        'role' => 'teacher',
        'averageMark' => 0 ,
        'subject' => 'php',
        'workingDay' => '19-21'
    ]
];


try{
    foreach ($members as $member){
    $sql = 'INSERT INTO members SET 
            full_name = "'.$member['fullName'].' ",
            phone = "'.$member['phone'].' ",
            email = "'.$member['email'].' ",
            role = "'.$member['role'].' ",
            average_mark = "'.$member['averageMark'].' ",
            subject = "'.$member['subject'].' ",
            working_day = "'.$member['workingDay'].' " ';
        $pdo->exec($sql);
    }

}catch (Exception $exception){
    echo 'Error storing member' .''. $exception->getMessage();
    die();
}

header('Location:index.php');


