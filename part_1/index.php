<?php
require_once 'Config/db.php';

try{
    $sql = 'SELECT * FROM members';
    $pdoResult = $pdo->query($sql);
    $membersArr = $pdoResult->fetchAll();
}catch(Exception $exception){
    echo "Error getting members " . $exception->getCode() . ' ' . $exception->getMessage();
    die();
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Main page</title>
</head>
<body>
    <div>
       <ul>
           <li>
               <a href="addMember.php">Add member</a>
           </li>
           <li>
               <a href="seeder_db.php">Fill in the table</a>
           </li>
       </ul>
    </div>
    <div>
        <table>
            <tr>
                <th>full name</th>
                <th>phone</th>
                <th>email</th>
                <th>role</th>
                <th>average mark</th>
                <th>subject</th>
                <th>working day</th>
            </tr>
            <?php foreach($membersArr as $member) :?>
                <tr>
                    <td><?=$member['full_name']?></td>
                    <td><?=$member['phone']?></td>
                    <td><?=$member['email']?></td>
                    <td><?=$member['role']?></td>
                    <td><?=$member['average_mark']?></td>
                    <td><?=$member['subject']?></td>
                    <td><?=$member['working_day']?></td>
                    <td>
                        <form action="delete.php" method="post">
                            <input type="hidden" name="member_id" value="<?=$member['id']?>">
                            <button>Delete</button>
                        </form>
                    </td>
                </tr>

            <?php endforeach;?>
        </table>
    </div>

</body>
</html>
