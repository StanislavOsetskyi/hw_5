<?php
require_once '../Config/db.php';
require_once '../Classes/Person.php';
require_once '../Classes/Student.php';

echo '<pre>Students</pre>';
try{
    $sql = 'SELECT * FROM members';
    $pdoResult = $pdo->query($sql);
    $membersArr = $pdoResult->fetchAll();
}catch(Exception $exception){
    echo "Error getting members " . $exception->getCode() . ' ' . $exception->getMessage();
    die();
}

foreach ($membersArr as $member){
    if ($member['role']  =="student ") {
        $student = new Student($member['full_name'], $member['phone'], $member['email'],
            $member['role'], $member['average_mark']);
        echo '<pre>';
        echo $student->getVisitCard();
    }
}