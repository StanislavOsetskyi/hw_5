<?php
require_once '../Config/db.php';
require_once '../Classes/Person.php';
require_once '../Classes/Teachers.php';

echo '<pre>Teachers</pre>';
try{
    $sql = 'SELECT * FROM members';
    $pdoResult = $pdo->query($sql);
    $membersArr = $pdoResult->fetchAll();
}catch(Exception $exception){
    echo "Error getting members " . $exception->getCode() . ' ' . $exception->getMessage();
    die();
}

foreach ($membersArr as $member){
    if ($member['role']  =="teacher ") {
        $teacher = new Teachers($member['full_name'], $member['phone'], $member['email'],
            $member['role'], $member['subject']);
        echo '<pre>';
        echo $teacher->getVisitCard();
    }
}