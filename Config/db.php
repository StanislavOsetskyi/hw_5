<?php
$host = 'localhost';
$dbuser = 'root';
$dbpassword = '';
$dbname = 'hw_5';

try{
    $pdo = new PDO('mysql:host='.$host. ';dbname='.$dbname, $dbuser, $dbpassword);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(Exception $exception){
    echo 'Error db connection'. $exception->getMessage();
    die();
}