<?php
require_once '../Config/db.php';

class Admin extends Person
{
    public $workDay = 'Monday';

    public function __construct($fullName, $phone, $email, $role, $workDay)
    {
        parent::__construct($fullName, $phone, $email, $role);
        $this->workDay = $workDay;
    }

    public function getVisitCard()
    {
        $str =parent::getVisitCard();
        $str .= $this->workDay;
        return $str;
    }
}