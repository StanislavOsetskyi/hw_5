<?php
require_once '../Config/db.php';

class Teachers extends Person
{
    public $subject;

    public function __construct($fullName, $phone, $email, $role,$subject)
    {
        parent::__construct($fullName, $phone, $email, $role);
        $this->subject = $subject;
    }

    public function getVisitCard()
    {
        $str = parent::getVisitCard();
        $str .= $this->subject;
        return $str;
    }
}