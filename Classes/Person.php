<?php
require_once '../Config/db.php';

class Person
{
    public $fullName = '';
    public $phone = 1;
    public $email = '';
    public $role = '';

    public function __construct($fullName,$phone,$email,$role)
    {
        $this->fullName = $fullName;
        $this->phone = $phone;
        $this->email = $email;
        $this->role = $role;
    }

    public function getVisitCard(){
        $str = '';
        $str.= $this->fullName. ',';
        $str.= $this->phone. ',';
        $str.= $this->email. ',';
        $str.= $this->role. ',';
        return $str;
    }
}