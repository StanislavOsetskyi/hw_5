<?php
require_once '../Config/db.php';


class Student extends Person
{
    public $averageMark;

    public function __construct($fullName, $phone, $email, $role,$averageMark)
    {
        parent::__construct($fullName, $phone, $email, $role);
        $this->averageMark = $averageMark;
    }

    public function getVisitCard()
    {
        $str = parent::getVisitCard();
        $str .= $this->averageMark;
        return $str;
    }
}

